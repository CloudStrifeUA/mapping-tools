# PyVMF
A python VMF parser grabbed from [here](https://github.com/GorangeNinja/PyVMF) and fixed a few bugs.

# Get Uscale
**Python 3.7 or higher required**<br />
Prints key-value map to the console, where **key** is a prop_static`s model name and **value** is a list of custom scale factors that applied to this model.
#### Usage
`python get_uscale.py <vmf>`