import sys
sys.path.append('./PyVMF')
from PyVMF import *

# def replace_last(source_string, replace_what, replace_with):
    # head, _sep, tail = source_string.rpartition(replace_what)
    # return head + replace_with + tail

if len(sys.argv) != 2:
	print("Usage: python {0} <vmf>".format(sys.argv[0]))
	sys.exit(1)

vmf = load_vmf(sys.argv[1], 0)
entities = vmf.get_entities()
scales_map = dict()
count = 0;
for entity in entities:
	if entity.classname == "prop_static":
		if entity.uniformscale != 1:
			count += 1
			scale = str(entity.uniformscale)
			if entity.model in scales_map:
				scales_map[entity.model].append(scale)
			else:
				scales_map[entity.model] = [scale]
			# entity.model = replace_last(entity.model, '.', "_" + scale.replace('.', '_') + ".")
# vmf.export(replace_last(sys.argv[1], '.', "_e."))
print(scales_map)
print("Totaly {0} prop_static models with custom uniformscale.".format(count))